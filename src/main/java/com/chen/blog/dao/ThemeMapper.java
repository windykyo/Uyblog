package com.chen.blog.dao;

import com.chen.blog.pojo.Theme;

public interface ThemeMapper {

    Theme select();

    int update(Theme theme);
}